#!/usr/bin/env python
# coding: utf-8

# # поиск режимов - полок

# In[12]:


if 'pd' not in globals():
    import pandas as pd
    #print('import pandas as pd')
if 'np' not in globals():
    import numpy as np
    #print('import numpy as np')
if 'xlrd' not in globals():
    import xlrd
    #print('import xlrd')

if 'os' not in globals():
    import os

if 'glob' not in globals():
    from glob import glob
    
pathArch = r"D:\РД191_ARCH_onD"
pathConf = r"D:\python\conf"

pathSpravPrms =  pathConf +r'\191_Справочник_параметров.xlsx'
pathSpravReg = pathConf +r'\Справочник_режимов.xlsx'

pathMeanDopusk = pathConf + r'\Допуска_переменных_на_режимах.xlsx'


# In[2]:


# поиск режимов - полок
# на входе pd.DataFrame.  index - время, и две голонки регулиятор и дроссель
#     %store -r regDrCmd
#     searchRegims(regDrCmd)
def searchRegims(regDrCmd):
    # поиск режимов - полок
    tmp0 = regDrCmd.rename(columns = {regDrCmd.columns[0]:'delta_AL11',regDrCmd.columns[1]:'delta_AL22'})

    tmp1 = tmp0.shift(-1) - tmp0

    tmp1.loc[ ( (tmp1.delta_AL11.apply(abs) > 0.001) | (tmp1.delta_AL22.apply(abs) > 0.001) ), 'regime'] = -1

    tmp2 = tmp0 - tmp0.shift(1) 

    tmp2['tt2'] = tmp2.index

    tmp2 = tmp2.shift(-1)
    
    tmp2.loc[ ( (tmp2.delta_AL11.apply(abs) > 0.001) | (tmp2.delta_AL22.apply(abs) > 0.001) ), 'regime'] = -1
    
    tmp1['tt1'] = tmp0.index

    
    tmp3 = pd.merge(tmp1,tmp2[['tt2']],left_index=True,right_index=True,suffixes=('', '_y')).shift(1) # смещпаем интервал после выданной команды
   

    #tmp3['t1'] = tmp3.index
    del tmp0, tmp2

    #Находим дельту между началом интревалом и концом
    tmp3.loc[ tmp3.regime.notna(),'ddt']  = tmp3[tmp3.regime.notna()].tt1.shift(-1) -   tmp3[tmp3.regime.notna()].tt2

    #Поправка на движение приводов и время прохождения команды
    tmp3['mx_sec'] = tmp3[['delta_AL11','delta_AL22']].apply(lambda x: max(abs(x.delta_AL11),abs(x.delta_AL22))*0.004 + 0.050,axis=1)

  
    tmp3['t0'] = tmp3.index 
    tmp3['t1'] = tmp3.index + tmp3['mx_sec']
    tmp3['t2'] = tmp3.index + tmp3.ddt

    # сдвиг т1 на 1 сек если найденный интервал больше 1 сек иначе сдвиг на 0.5
    tmp3['t3'] =  tmp3.apply(lambda x: x.t1 + 1 if (x.t2-x.t1)>=1 else x.t1 + 0.4, axis =1)
    tmp3['t4'] = tmp3['t2']
    
#     #Выбираем интервалы
#     tmp3['vibor'] = tmp3.apply(lambda x: True if  ( 
#         ((x.t1 < 9) and (x.dt >=0.5)) or(
#         ((x.t1 >= 10) and (x.dt >=1))
#         ) else False)
    

    tmp3['vibor'] = tmp3.apply(lambda x: True if ( (x.t1 > 1) and (x.t1 < 9) and (x.ddt >=0.5) ) or ((x.t1 >= 9) and (x.ddt >=1.5)) else False,axis=1)

    regims = tmp3[tmp3['vibor']].reset_index()[['t1','t2','t3','t4','t0']]
    
    #Добавляем первую строку
    regims = pd.DataFrame(pd.Series([np.nan,0.0,0.0,0.0,0.0,0.0], index=['regime','t1', 't2', 't3', 't4', 't0'])).T.append(regims,ignore_index=True)

    # коррекция для последнего режима ( по факту заканчивается позже)
    regims.loc[regims[-1:].index[0], 't2'] = regims.loc[regims[-1:].index[0], 't2'] - 0.2
    regims.loc[regims[-1:].index[0], 't4'] = regims.loc[regims[-1:].index[0], 't2']
    return regims.round(3)
  


# # Загрузка стандартных режимов и поиск имен

# In[3]:


def readSpravRegims():
    # Открываем режимы по типам dfRegs
    # Поиск `стандартных режимов

    rb = xlrd.open_workbook(pathSpravReg) #,formatting_info=True)
    sheet = rb.sheet_by_index(0)
    if(sheet.row_values(0)[0] != 'Список режимов по типам испытаний. Сохраняйте порядок строк и колонок') : raise Exception(sheet.row_values(0)[0])  
    rs = []
    for rownum in range(sheet.nrows):
        if rownum == 0 : continue
        row = sheet.row_values(rownum)
        rs.append(row)
    dfRegs = pd.DataFrame(rs[1:], columns=rs[0])
    dfRegs.id = dfRegs.id.astype(int)
    # name = sheet.row_values(0)[1].replace('№','')
    # df = pd.DataFrame(sheet.row_values(3)[3:])
    # df['t1'] = pd.DataFrame(sheet.row_values(4)[3:])
    del rb
    del sheet
    return dfRegs


# In[4]:


#     %store -r regDrCmd
#     %store -r path
#     typeRegim = 'auto'
# path = 'tmp\\222_D058_'
def searchRegimNames(regDrCmd, typeRegim, path, debug = False):
    dfRegs = readSpravRegims()
    regims = searchRegims(regDrCmd)
    
    global len_regims_no_name
    len_regims_no_name = len(regims)
    
    outNames = ['regims_no_names.xlsx', 'regims_sprav.xlsx', 'regims_names.xlsx']
    if debug : regims.to_excel(path + outNames[0])
    # Если auto ищем подходящий вид испытания

    if typeRegim == 'auto':
        for ind in  dfRegs.index:
            dfRegs.loc[ind,'poisk'] = False
            for rind in regims.index:
                 if abs(dfRegs.loc[ind,'t2'] - regims.loc[rind,'t2']) <= 0.25: dfRegs.loc[ind,'poisk'] = True

        coefs =  dfRegs.groupby('Тип испытания').poisk.sum()/dfRegs.groupby('Тип испытания').poisk.count()
        typeRegim= coefs.sort_values().index[-1]

    regims_kti =  dfRegs[dfRegs['Тип испытания'] == typeRegim].copy()
    
    regims_kti

    global len_regims_kti 
    len_regims_kti = len(regims_kti)

    regims_kti.loc[regims_kti['Rnom']!='','RДопуск1'] = regims_kti.loc[(regims_kti['Rnom']!='') & (regims_kti['RДопуск-']!=''),'Rnom'].astype(float) + regims_kti.loc[(regims_kti['Rnom']!='') & (regims_kti['RДопуск-']!=''),'RДопуск-'].astype(float)
    regims_kti.loc[regims_kti['Rnom']!='','RДопуск2'] = regims_kti.loc[(regims_kti['Rnom']!='') & (regims_kti['RДопуск+']!=''),'Rnom'].astype(float) + regims_kti.loc[(regims_kti['Rnom']!='') & (regims_kti['RДопуск+']!=''),'RДопуск+'].astype(float)

    vibor = (regims_kti['Kmnom']!='') & (regims_kti['KmДопуск-']!='') & (regims_kti['KmДопуск+']!='') & (~regims_kti['Kmnom'].isna() ) & (~regims_kti['KmДопуск-'].isna() ) & (~regims_kti['KmДопуск+'].isna() )

    regims_kti.loc[vibor,'KmДопуск1'] = regims_kti.loc[vibor,'KmДопуск-'].astype(float) * regims_kti.loc[vibor,'Kmnom'].astype(float) + regims_kti.loc[vibor,'Kmnom'].astype(float)
    regims_kti.loc[vibor,'KmДопуск2'] = regims_kti.loc[vibor,'KmДопуск+'].astype(float) * regims_kti.loc[vibor,'Kmnom'].astype(float) + regims_kti.loc[vibor,'Kmnom'].astype(float)

    regims_kti.loc[regims_kti['Kmnom']!='','Km_nomf'] = regims_kti.loc[(regims_kti['Kmnom']!='') ,'Kmnom'].astype(float) 

    regims_kti.reset_index(inplace=True,drop=True)#.set_index('Имя',inplace = True)
    if debug : regims_kti.to_excel(path +  outNames[1])

    
## поиск имен режимов
    dopregims = pd.DataFrame()
    
    for reg in regims.index:
        t1 = regims.loc[reg,'t1'] #найденные t1, t2
        t2 = regims.loc[reg,'t2']
        
        ser = regims_kti.t2 - t2 # серия из разниц по времени
        ffind = ser[abs(ser) <= 0.25]

        regims.loc[reg,'id'] = -1 if len(ffind) == 0 else regims_kti.loc[ffind.index[0],'id']
        regims.loc[reg,'name'] = '' if len(ffind) == 0 else regims_kti.loc[ffind.index[0],'Имя']
        regims.loc[reg,'Rdop1'] = None if len(ffind) == 0 else regims_kti.loc[ffind.index[0],'RДопуск1']
        regims.loc[reg,'Rdop2'] = None if len(ffind) == 0 else regims_kti.loc[ffind.index[0],'RДопуск2']
        regims.loc[reg,'Km_dop1'] = None if len(ffind) == 0 else regims_kti.loc[ffind.index[0],'KmДопуск1']
        regims.loc[reg,'Km_dop2'] = None if len(ffind) == 0 else regims_kti.loc[ffind.index[0],'KmДопуск2']
        
        regims.loc[reg,'etalon_t1'] = None if len(ffind) == 0 else regims_kti.loc[ffind.index[0],'t1']
        regims.loc[reg,'etalon_t2'] = None if len(ffind) == 0 else regims_kti.loc[ffind.index[0],'t2']
        regims.loc[reg,'etalon_t3'] = None if len(ffind) == 0 else regims_kti.loc[ffind.index[0],'t3']
        regims.loc[reg,'etalon_t4'] = None if len(ffind) == 0 else regims_kti.loc[ffind.index[0],'t4']
        #regims_kti[ffind.index[0]]
        
        if len(ffind) > 0 : regims_kti.loc[ffind.index[0],'lock'] = True

        # бывают ситуации когда в справочнике есть режимы, которые не определяются попробуем их добавить, это случается когда режимы есть а привода не двигаются
        if  (len(ffind) > 0) and  (ffind.index[0] > 0) and abs(regims_kti.loc[ffind.index[0]].t1 - regims.loc[reg,'t1']) > 0.25 : #нашли
                        
            poisk_sb = regims_kti.lock.isna() & (abs(regims_kti.t1 - regims.loc[reg,'t1']) < 0.25)
            if poisk_sb.sum() != 1: 
                print('ERR - len(poisk_sb) != 1  ', regims.loc[reg,'name'])
                continue # анамалия
            
            curent_index = ffind.index[0]
            find_index = regims_kti[poisk_sb].index[0]
            
            if (~regims_kti.loc[find_index:curent_index].lock.isna()).sum() != 1  : 
                print('ERR  if (~regims_kti.loc[find_index:curent_index].lock.isna()).sum() != 1  : ')
                continue #должен быть только один режим с lock
            
            temp_t1 = regims.loc[reg,'t1']
            temp_t3 = regims.loc[reg,'t3']
            #коррекция времени
            regims.loc[reg,'t1'] = regims_kti.loc[curent_index].t1
            regims.loc[reg,'t3'] = regims_kti.loc[curent_index].t3
            
            regims_notFind =  regims_kti.loc[find_index+1:curent_index-1]
            
            #добавляем режим с реальным временем t1 t3 
            ind = len(dopregims)
            dopregims.loc[ind, 't1'] = temp_t1
            dopregims.loc[ind, 't2'] = regims_kti.loc[find_index].t2
            dopregims.loc[ind, 't3'] = temp_t3
            dopregims.loc[ind, 't4'] = regims_kti.loc[find_index].t4
            dopregims.loc[ind,'id'] = -1 if len(ffind) == 0 else regims_kti.loc[find_index,'id']
            dopregims.loc[ind,'name'] = '' if len(ffind) == 0 else regims_kti.loc[find_index,'Имя']
            dopregims.loc[ind,'Rdop1'] = None if len(ffind) == 0 else regims_kti.loc[find_index,'RДопуск1']
            dopregims.loc[ind,'Rdop2'] = None if len(ffind) == 0 else regims_kti.loc[find_index,'RДопуск2']
            dopregims.loc[ind,'Km_dop1'] = None if len(ffind) == 0 else regims_kti.loc[find_index,'KmДопуск1']
            dopregims.loc[ind,'Km_dop2'] = None if len(ffind) == 0 else regims_kti.loc[find_index,'KmДопуск2']

            dopregims.loc[ind,'etalon_t1'] = None if len(ffind) == 0 else regims_kti.loc[find_index,'t1']
            dopregims.loc[ind,'etalon_t2'] = None if len(ffind) == 0 else regims_kti.loc[find_index,'t2']
            dopregims.loc[ind,'etalon_t3'] = None if len(ffind) == 0 else regims_kti.loc[find_index,'t3']
            dopregims.loc[ind,'etalon_t4'] = None if len(ffind) == 0 else regims_kti.loc[find_index,'t4']
            
            #добавляем прочие режимы из справочника
            for r_i in regims_notFind.index:
                ind = len(dopregims)
                dopregims.loc[ind, 't1'] = regims_notFind.loc[r_i].t1
                dopregims.loc[ind, 't2'] = regims_notFind.loc[r_i].t2
                dopregims.loc[ind, 't3'] = regims_notFind.loc[r_i].t3
                dopregims.loc[ind, 't4'] = regims_notFind.loc[r_i].t4
                dopregims.loc[ind,'id'] =  regims_notFind.loc[r_i,'id']
                dopregims.loc[ind,'name'] = regims_notFind.loc[r_i,'Имя']
                dopregims.loc[ind,'Rdop1'] = regims_notFind.loc[r_i,'RДопуск1']
                dopregims.loc[ind,'Rdop2'] = regims_notFind.loc[r_i,'RДопуск2']
                dopregims.loc[ind,'Km_dop1'] = regims_notFind.loc[r_i,'KmДопуск1']
                dopregims.loc[ind,'Km_dop2'] = regims_notFind.loc[r_i,'KmДопуск2']

                dopregims.loc[ind,'etalon_t1'] = regims_notFind.loc[r_i,'t1']
                dopregims.loc[ind,'etalon_t2'] = regims_notFind.loc[r_i,'t2']
                dopregims.loc[ind,'etalon_t3'] = regims_notFind.loc[r_i,'t3']
                dopregims.loc[ind,'etalon_t4'] = regims_notFind.loc[r_i,'t4']
                
                
        
    if len(dopregims) >0 : regims = pd.concat([regims,dopregims]).sort_values(['t1','t2']).reset_index(drop=True)
        
    regims.id = regims.id.astype(int)

        #'MassFlow_nom_sum', 'RДопуск-',
       #'RДопуск+', 'Kmnom', 'KmДопуск-', 'KmДопуск+'
    
    #Добавление режимов PHTO
    st = regims_kti['Имя'].str.count('PHTO') != 0
    
    for ind in st[st].index:
        regims = regims.append({
            'id':regims_kti.loc[ind,'id'],
            'name':regims_kti.loc[ind,'Имя'],
            't0' : regims_kti.loc[ind,'t1'],
            't1' : regims_kti.loc[ind,'t1'],
            't2' : regims_kti.loc[ind,'t2'],
            't3' : regims_kti.loc[ind,'t3'],
            't4' : regims_kti.loc[ind,'t4'],

            'Rdop1':  regims_kti.loc[ind,'RДопуск1'],
            'Rdop2':  regims_kti.loc[ind,'RДопуск2'],

            'Km_dop1':  regims_kti.loc[ind,'KmДопуск1'],
            'Km_dop2':  regims_kti.loc[ind,'KmДопуск2'],

            'etalon_t1' : regims_kti.loc[ind,'t1'],
            'etalon_t2' : regims_kti.loc[ind,'t2'],
            'etalon_t3' : regims_kti.loc[ind,'t3'],
            'etalon_t4' : regims_kti.loc[ind,'t4'],

        },ignore_index=True)
   
    

    cnt = 1
    for r in regims.index:
        t3,t4 = regims.loc[r,['t3','t4']]
        if t3==t4 : continue
        regims.loc[r,'regime'] = str(cnt) #str(round(t4,1) )
        cnt += 1
    
    if debug : regims_kti.to_excel(path + outNames[2])

    global blocksSearchRegims
    blocksSearchRegims = '<div style="width:auto">' #; background:#CCCCCC
    blocksSearchRegims += 'Режимы из справочника <i><b>"%s"</b></i> <b>%d</b> шт. </br>'%(typeRegim, len(regims_kti))
    blocksSearchRegims += 'Найдено режимов <b>%d</b> шт. :'%(len(regims))
    if debug : blocksSearchRegims += '<a href="%s" target="_blank">%s</a> ---> '%(path+outNames[0],outNames[0])
    if debug : blocksSearchRegims += '<a href="%s" target="_blank">%s</a> --->'%(path+outNames[1],outNames[1])
    if debug : blocksSearchRegims += '<a href="%s" target="_blank">%s</a> <br>'%(path+outNames[2],outNames[2])
    blocksSearchRegims += 'Из них совпало  <b>%d</b> шт.<br>'%((regims.name != '').sum())
    blocksSearchRegims += '</div>'

    return regims


# # Работа со справочником параметров

# In[5]:


def readPrmsSprav():
    rb = xlrd.open_workbook(pathSpravPrms) #,formatting_info=True)
    sheet = rb.sheet_by_index(0)
    if(sheet.row_values(3)[0] != 'Сегмент') : raise Exception('!= Сегмент')  
    sprav191 =  pd.DataFrame()
    for  i,col in enumerate(sheet.row_values(3)):
        sprav191[col] =  sheet.col_values(i)[4:]
    sprav191
    sprav = sprav191.iloc[:,2:8]
    sprav
    sprav.columns = ['kod','name','eu','exclude','dop1','dop2']
    sprav['kod'] = sprav['kod'].astype(int)
    sprav['exclude'] = sprav['exclude'].apply(lambda x: False if x=='' else True)
   
    return sprav


# In[6]:


def readIspitExclude():
    # Открываем режимы по типам dfRegs
    # Поиск `стандартных режимов

    rb = xlrd.open_workbook(pathSpravPrms) #,formatting_info=True)
    sheet = rb.sheet_by_index(1)
    if(sheet.row_values(1)[0] != 'Исключить из обработки следующие испытания:') : raise Exception(sheet.row_values(1)[0])  
    rs = []
    for rownum in range(sheet.nrows):
        if rownum == 0 : continue
        row = sheet.row_values(rownum)
        rs.append(row)
    dfIspitExclude = pd.DataFrame(rs[3:], columns=['ispit','comment'], dtype =np.int32)
    return dfIspitExclude


# In[7]:


def readPrmsExclude():
    # Открываем режимы по типам dfRegs
    # Поиск `стандартных режимов

    rb = xlrd.open_workbook(pathSpravPrms) #,formatting_info=True)
    sheet = rb.sheet_by_index(2)
    if(sheet.row_values(1)[0] != 'Исключить из обработки следующие сбойные параметры:') : raise Exception(sheet.row_values(1)[0])  
    rs = []
    for rownum in range(sheet.nrows):
        if rownum == 0 : continue
        row = sheet.row_values(rownum)
        rs.append(row)
    dfExclude = pd.DataFrame(rs[3:], columns=['ispit','prm','comment'], dtype =np.int32)
    return dfExclude


# In[24]:


def readMeanDopusk():
    rb = xlrd.open_workbook(pathMeanDopusk) #,formatting_info=True)
    sheet = rb.sheet_by_index(0)
    if sheet.row_values(0)[1:3] != ['kod', 'regime'] : raise Exception(sheet.row_values(0)[1:3])  
    rs = []
    for rownum in range(sheet.nrows):
        #if rownum == 0 : continue
        rs.append(sheet.row_values(rownum)[1:])
    dfExclude = pd.DataFrame(rs[1:], columns=['kod', 'regime', 'variable', 'v_min', 'v_max', 'ispits'], dtype =np.int32)
    dfExclude
    return dfExclude


# In[8]:



# def readPreRegimsFromPtocol():
#     # Открываем режимы по типам dfRegs
#     # Поиск `стандартных режимов

#     rb = xlrd.open_workbook(pathConf+"\\Режимы_по_протоколам.xlsx") #,formatting_info=True)
#     sheet = rb.sheet_by_index(3)
#     if(sheet.row_values(1)[0] != 'Список режимов из протоколов прошедших испытаний для определения речки допусков. Сохраняйте порядок строк и колонок') : raise Exception(sheet.row_values(1)[0])  
#     rs = []
#     for rownum in range(sheet.nrows):
#         if rownum == 0 : continue
#         row = sheet.row_values(rownum)
#         rs.append(row)
#     df_readPreRegimsFromPtocol = pd.DataFrame(rs[3:], columns=['regime','t1','t2','t3','t4','ispit'])
#     df_readPreRegimsFromPtocol['ispit'] = df_readPreRegimsFromPtocol['ispit'].astype(int)
#     return df_readPreRegimsFromPtocol


# ## Формирование режимов по протоколам

# In[9]:


def regimsFromProtocols():
    xlsFiles = list(glob(os.path.join(pathArch, '*.xls*')))
    xlsFiles

    dfs = []
    for xlsf in xlsFiles: 
        #print(xlsf)
        rb = xlrd.open_workbook(xlsf) #,formatting_info=True)
        sheet = rb.sheet_by_index(-1)
        if not sheet.row_values(0)[1].__contains__('№') : raise Exception(sheet.row_values(0)[1])  

        ni = int(sheet.row_values(0)[1].replace('№',''))

        res = pd.DataFrame( [ 
        sheet.row_values(2)[3:],
        sheet.row_values(3)[3:],
        sheet.row_values(4)[3:],
        sheet.row_values(5)[3:],
        sheet.row_values(6)[3:],
        sheet.row_values(7)[3:],
        [ni]*len(sheet.row_values(7)[3:])
        ] ).T

        res.columns = ['kod','regime','t1','t2','t3','t4','ispit']
        
        res.kod = res.kod.replace("'",'').astype(int)

        dfs.append(res.copy())
        del res
    res = pd.concat(dfs)
    res.Name = 'Режимы по протоколам'
    res.to_excel(pathConf+"\\Режимы_по_протоколам.xlsx")
    return res


# # Работа с b.b-файлами

# In[10]:


def readBBfile(bbPath):
    sprav = readPrmsSprav()
    kods = sprav.set_index('kod').name.to_dict()

    def getNameByKod(kod):
        if kod in kods:    
            return kods[kod]
        else:
            return '__'
    import struct

    nachaloBloka = b'\xff\x88\x91\x8f\x9b\x92.\x9d\x93'
    
    
    def searchBlocks(data):
        res = []
        res.append(data.find(nachaloBloka))
        while (res[-1] != -1) and len(res) > 0 :
            res.append( data.find(nachaloBloka,res[-1]+1) )
        return res[:-1] if len(res)>0 else res
    
    
    def readBlock(inind,data, debug = False):


        #     inind = blocksaddr[2]
        #     ind  = inind
        #     debug = True
        ind  = inind

        # #  unsigned  char tg; // const FF               0
        # # 		    char       tgs[8]; // const ИСПЫТ_ЭУ         1
        # # 		    unsigned char tgt; // Тип блока              9
        # # 		    char       nEU[8]; // Тип(Имя) ЭУ           10
        # # 		    char       nNI[4]; // Номер испытания       18
        # # 		    char       nST[2]; // Номер стенда          22
        # # 		    char    timei[18]; // Дата проведения НИ    24
        # # 				       // mm.dd.yy hh:mm:ss
        # # 		    char    timeo[18]; // Дата обработки НИ     42
        # # 		    char      nSGM[2]; // Номер сегмента        60
        # # 		    short        nNIi; // Номер испытания int   62
        # # 		    short       nSGMi; // Номер сегмента  int   64
        # # 		    short        nGRP; // Номер группы  int     66
        # # 		    short        npar; // количество параметров 68
        # # 		    def_par param[32]; // параметры             70
        # # 									  // 582байта
        group = {}
        if (len(data) < 582) or (ind + 9 >= len(data)) or (data[ind:ind+9] != nachaloBloka):return group

        if data[ind] != 255 : raise Exception('data[0] != 255') 
        if (debug) : print('char tg; // const FF               0',ind,':',data[ind])
        ind+=1

        group['tgs'] = data[ind:ind+8].decode('cp866')
        if (debug) : print('char       tgs[8]; // const ИСПЫТ_ЭУ         1',ind,':',group['tgs'])
        ind+=8

        group['tgt'] = int.from_bytes([data[ind]], byteorder='big') 
        if (debug) : print('unsigned char tgt; // Тип блока              9',ind,':',group['tgt'])
        ind=ind+1


        group['nEU'] = data[ind:ind+8].decode('cp866').strip()
        if (debug) : print('char       nEU[8]; // Тип(Имя) ЭУ           10',ind,':',group['nEU'])
        ind +=8

        group['nNI'] = data[ind:ind+4].decode('cp866').strip()#int(str(data[ind:ind+4]).replace("'",'').replace("b",''))
        if (debug) : print('char       nNI[4]; // Номер испытания       18',ind,':',group['nNI'])
        ind +=4

        group['nST'] = data[ind:ind+2].decode('cp866').strip()#int(str(data[ind:ind+2]).replace("'",'').replace("b",''))
        if (debug) : print('char       nST[2]; // Номер стенда          22',ind,':',group['nST'])
        ind +=2


        group['timei'] = data[ind:ind+18].decode('cp866').strip()
        if (debug) : print('char    timei[18]; // Дата проведения НИ    24',ind,':',group['timei'])
        ind +=18

        group['timeo'] = data[ind:ind+18].decode('cp866').strip()
        if (debug) : print('char    timeo[18]; // Дата обработки НИ     42',ind,':',group['timeo'])
        ind +=18

        group['nSGM'] = data[ind:ind+2].decode('cp866').strip()#int(str(data[ind:ind+2]).replace("'",'').replace("b",''))
        if (debug) : print('char      nSGM[2]; // Номер сегмента        60',ind,':',group['nSGM'])
        ind +=2


        group['nNIi'] = int.from_bytes(data[ind:ind+2], byteorder='little', signed=True)
        if (debug) : print('short        nNIi; // Номер испытания int   62',ind,':',group['nNIi'])
        ind +=2

        group['nSGMi'] = int.from_bytes(data[ind:ind+2], byteorder='little', signed=True)
        if (debug) : print('short       nSGMi; // Номер сегмента  int   64',ind,':',group['nSGMi'])
        ind +=2

        group['nGRP'] = int.from_bytes(data[ind:ind+2], byteorder='little', signed=True)
        if (debug) : print('short        nGRP; // Номер группы  int     66',ind,':',group['nGRP'])
        ind +=2

        group['npar'] = int.from_bytes(data[ind:ind+2], byteorder='little', signed=True)
        if (debug) : print('short        npar; // количество параметров 68',ind,':',group['npar'])
        ind +=2


        group['param'] = [] 

        for i in range(group['npar']):
            group['param'].append( { 'name' : data[ind + 16*i:ind +16*i+ 12].decode('cp866').strip(), 
                           'kod' : int.from_bytes(data[ind+16*i+12:ind+16*i+12+4], byteorder='little') })
            #ind += 16
        if (debug) : print('def_par param[32]; // параметры             70',ind,':',group['param'])



        if (group['nGRP'] == 6) or (group['nGRP'] == 7) : 
            group['npar'] = 0
            group['data'] = []
            return group
            #raise Exception('(nGRP == 6) or (nGRP == 7) ')
        #582байта
        ind = inind + 582

        #for i in range(npar):

        tt = [0.0]
        paramslst = []
        while (tt[0] != 5555.0) and (ind + 4 < len(data)):
            tt = struct.unpack('f', data[ind:ind+4])
            ind +=4
            if tt[0] == 5555.0: break
            paramslst.append( tt + struct.unpack('f'*(group['npar']), data[ind:ind+4*(group['npar'])]) )
            #print('Данные:',ind,':',tt,paramst)
            ind +=group['npar']*4
        group['data'] = paramslst
        if debug : print('Кол-во записей с данными по группе:',len(paramslst))
        return group
    
    
    with open(bbPath, "rb") as binary_file:  
        data = binary_file.read()
    blocksaddr = searchBlocks(data)
    res = []
    
    for blAdr in blocksaddr:
        bl = readBlock(blAdr,data)
        if bl['npar'] > 0 :
            
            columns = ['tt'] + [  getNameByKod(x['kod']) for x in bl['param'] ]
            tmp = pd.DataFrame(bl['data'], columns = columns ).drop_duplicates().groupby('tt').last()
            if '__' in  tmp.columns :
                tmp.drop(columns='__',inplace = True)
            tmp = tmp.melt(ignore_index=False)
            tmp['sg'] = bl['nSGMi']
            tmp['ni'] = bl['nNIi']
            res.append(tmp.copy())
            del tmp
            
    bb_df = pd.concat(res)
    return bb_df


# In[25]:


def readAll(bbPath):
    allRegims = regimsFromProtocols()
    bb_df = readBBfile(bbPath)
    ni = bb_df.ni.iloc[0]

    sgs = bb_df.sg.drop_duplicates().values.tolist()
    res = allRegims[allRegims.ispit == ni].copy()
    if len(res) == 0: raise Exception('len(res)')
    res

    res['t4_edit'] = res.apply( lambda x: x.t4 if x.t3!=x.t4 else 0.2, axis =1)

    for sg in sgs:
        filterData = bb_df[bb_df.sg==sg].pivot( columns='variable', values='value').rolling(5).median().fillna(method='backfill')

        for col in filterData.columns:
            res[col] = res.apply( lambda x: filterData.loc[ (filterData.index >= x.t3) & (filterData.index < x.t4_edit) ,col].mean() , axis = 1)

    return res.melt(id_vars=['kod','regime', 'ispit'], value_vars=res.columns.to_list()[8:])


# In[25]:


def readAll2(bbPath,allRegims):
    
    bb_df = readBBfile(bbPath)
    ni = bb_df.ni.iloc[0]

    sgs = bb_df.sg.drop_duplicates().values.tolist()
    res = allRegims[allRegims.ispit == ni].copy()
    if len(res) == 0: raise Exception('len(res)')
    res

    res['t4_edit'] = res.apply( lambda x: x.t4 if x.t3!=x.t4 else 0.2, axis =1)

    for sg in sgs:
        filterData = bb_df[bb_df.sg==sg].pivot( columns='variable', values='value').rolling(5).median().fillna(method='backfill')

        for col in filterData.columns:
            res[col] = res.apply( lambda x: filterData.loc[ (filterData.index >= x.t3) & (filterData.index < x.t4_edit) ,col].mean() , axis = 1)

    return res.melt(id_vars=['kod','regime', 'ispit'], value_vars=res.columns.to_list()[8:])

